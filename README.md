# src4reader

add description of src4reader here

## Usage Example

```
src4reader -l "*.hs/haskell" -l "*.cabal/cabal" -m | pandoc --standalone --toc -o haskell-relational-record-868f56fe7e0e0d52ba35e8f5a8168fddbd60f22c.html

src4reader -l "*.hs/haskell" -l "*.cabal/cabal" -l "*.sql/sql" -m | pandoc --latex-engine=xelatex --toc -o haskell-relational-record-868f56fe7e0e0d52ba35e8f5a8168fddbd60f22c.pdf
```
