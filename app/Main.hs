module Main where


import qualified Conduit as C
import           Conduit ((.|))

import           Documentation.Src4Reader


main :: IO ()
main = do
  args <- prepareArguments C.stdinC
  C.runConduitRes $ mainC args .| C.stdoutC
