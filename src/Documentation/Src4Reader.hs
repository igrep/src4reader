{-# LANGUAGE OverloadedStrings, RankNTypes #-}

module Documentation.Src4Reader
  ( mainC
  , prepareArguments
  ) where


import qualified Conduit as C
import           Conduit ((.|))
import qualified Data.ByteString.Char8 as BS
import qualified Data.List as L
import qualified Data.Maybe as M
import           Data.Monoid ((<>))
import qualified System.Directory as D

import           Documentation.Src4Reader.Arguments
import           Documentation.Src4Reader.SourceFile


mainC :: Arguments -> (C.MonadResource m) => C.Producer m BS.ByteString
mainC args = do
  entireFiles <- C.liftIO $
    map (fromFullPath args) . L.sort <$> C.runConduitRes (yieldPaths args .| C.sinkList)
  let files =
        if argumentsOnlyMatched args
          then filter (M.isJust . sourceFileLanguageName) entireFiles
          else entireFiles
  C.yieldMany files
    .| C.awaitForever toMarkdown


yieldPaths :: C.MonadResource m => Arguments -> C.Producer m FilePath
yieldPaths args = C.yieldMany (argumentsPaths args) .| C.awaitForever morePaths
  where
    followSymlinks = argumentsFollowSymlinks args
    morePaths path = do
      isDir <- C.liftIO $ D.doesDirectoryExist path
      if isDir
        then C.sourceDirectoryDeep followSymlinks path
        else C.yield path


toMarkdown :: C.MonadResource m => SourceFile -> C.Producer m BS.ByteString
toMarkdown sf =
  yieldSection $ M.fromMaybe "" $ sourceFileLanguageName sf
  where
    fullPath = sourceFileFullPath sf
    forToc = BS.pack $ sourceFileTocId sf
    yieldSection name = do
      C.yield $ "# " <> forToc <> "\n"
      C.yield $ "```" <> BS.pack name <> "\n"
      C.sourceFileBS fullPath
      C.yield "```\n"
