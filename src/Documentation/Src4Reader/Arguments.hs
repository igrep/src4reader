{-# LANGUAGE RankNTypes #-}

module Documentation.Src4Reader.Arguments
  ( prepareArguments
  , Arguments(argumentsTargetLanguages, argumentsOnlyMatched, argumentsFollowSymlinks, argumentsPaths, argumentsCurrentDirectory)
  ) where


import           Control.Applicative (many)
import qualified Control.Monad as M
import qualified Conduit as C
import           Conduit ((.|))
import           Data.Monoid ((<>))
import qualified Options.Applicative as OA
import           Options.Applicative.Builder ()
import qualified System.Directory as D
import           System.Exit (exitSuccess)

import qualified Paths_src4reader

import           Documentation.Src4Reader.Language


-- TODO: only-utf8 option (default true)
data Arguments =
  Arguments
    { argumentsTargetLanguages :: [Language]
    , argumentsOnlyMatched :: Bool
    , argumentsFollowSymlinks :: Bool
    , argumentsFromStdin :: Bool
    , argumentsPrintsVersion :: Bool
    , argumentsPaths :: [FilePath]
    , argumentsCurrentDirectory :: FilePath
    }


parser :: OA.Parser (FilePath -> Arguments)
parser =
  let langOpt = OA.option (OA.eitherReader parseLang)
        $ OA.long "language"
        <> OA.short 'l'
        <> OA.metavar "LANGUAGE"
        <> OA.help "Languages' pattern[/name]. e.g. '*.hs/haskell' '*.txt'"
      onlyMatchedOpt = OA.switch
        $ OA.long "only-matched"
        <> OA.short 'm'
        <> OA.help "Print only files matched with some of --language options."
      symlinkOpt = OA.switch
        $ OA.long "follow-symlinks"
        <> OA.short 's'
        <> OA.help "Follow symlinks (Default: false)"
      stdinOpt = OA.switch
        $ OA.long "stdin"
        <> OA.short 'i'
        <> OA.help "Read target files from stdin, separated by newlines. Intended to be used with 'git ls-files' or other commands"
      versionOpt = OA.switch
        $ OA.long "version" <> OA.short 'v' <> OA.help "Print version and exits."
      cmdArgs = many (OA.argument OA.str $ OA.metavar "FILES_OR_DIRECTORIES...")
  in Arguments <$> many langOpt <*> onlyMatchedOpt <*> symlinkOpt <*> stdinOpt <*> versionOpt <*> cmdArgs


parserInfo :: OA.ParserInfo (FilePath -> Arguments)
parserInfo =
  OA.info
    (OA.helper <*> parser)
    ( OA.fullDesc
        <> OA.progDesc "Prints the markdown containing file contents under the PATHs to stdout."
        <> OA.header (appName ++ " - Pack source codes into a markdown file.")
    )


prepareArguments :: C.Producer IO String -> IO Arguments
prepareArguments stdinC = do
  arguments <- OA.execParser parserInfo <*> D.getCurrentDirectory

  M.when (argumentsPrintsVersion arguments) $ do
    putStrLn $ appName ++ " - " ++ show Paths_src4reader.version
    exitSuccess

  let isFromStdin = argumentsFromStdin arguments
      paths = argumentsPaths arguments
      pathsC =
        if null paths && not isFromStdin
          then C.yield $ argumentsCurrentDirectory arguments
          else do
            M.when isFromStdin $ stdinC .| C.linesUnboundedC
            C.yieldMany paths
  allPaths <- C.runConduit $ pathsC .| C.sinkList
  return arguments { argumentsPaths = allPaths }


appName :: String
appName = "src4reader"
