module Documentation.Src4Reader.Language
  ( Language
  , matchAnyWithPath
  , parseLang
  ) where

import qualified Data.List as L
import qualified System.FilePath as FP
import qualified System.FilePath.Glob as G


data Language =
  Language
    { languagePattern :: G.Pattern
    , languageName :: String
    }


parseLang :: String -> Either String Language
parseLang s =
    case L.break (== '/') s of
        ("", "") -> Left $ "No Language specified: '" ++ s ++ "'"
        ("", _) ->  Left $ "Language file name pattern must not be empty: '" ++ s ++ "'"
        (p, n)  -> do
          ptn <- G.tryCompileWith G.compDefault p
          return $ Language ptn $ withoutHeadSlash n


withoutHeadSlash :: String -> String
withoutHeadSlash ('/':s) = s
withoutHeadSlash s = s


matchAnyWithPath :: FilePath -> [Language] -> Maybe String
matchAnyWithPath path langs = languageName <$> L.find p langs
  where p l = G.match (languagePattern l) $ FP.takeFileName path
