module Documentation.Src4Reader.SourceFile
  ( SourceFile(sourceFileFullPath, sourceFileTocId, sourceFileLanguageName)
  , fromFullPath
  ) where


import qualified System.FilePath as FP

import           Documentation.Src4Reader.Arguments
import           Documentation.Src4Reader.Language


data SourceFile =
  SourceFile
    { sourceFileFullPath :: FilePath
    , sourceFileTocId :: FilePath
    , sourceFileLanguageName :: Maybe String
    }


fromFullPath :: Arguments -> FilePath -> SourceFile
fromFullPath args fp =
  SourceFile fp
    (separateBySlash $ FP.makeRelative (argumentsCurrentDirectory args) fp)
    (matchAnyWithPath fp $ argumentsTargetLanguages args)


separateBySlash :: FilePath -> FilePath
separateBySlash = map slashWhenBackSlash
  where
    slashWhenBackSlash '\\' = '/'
    slashWhenBackSlash c = c
