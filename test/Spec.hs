{-# LANGUAGE OverloadedStrings #-}

import qualified Conduit as C
import           Conduit ((.|))
import           Control.Monad (forM)
import qualified Data.ByteString.Char8 as BS
import qualified Data.Map as M
import           Data.Monoid ((<>))
import qualified System.Directory as D
import qualified System.Environment as E
import           Test.Hspec hiding (shouldReturn)
import           Test.Hspec.Expectations.Pretty

import qualified Paths_src4reader

import           Documentation.Src4Reader.Arguments
import           Documentation.Src4Reader


main :: IO ()
main = do
  D.setCurrentDirectory =<< Paths_src4reader.getDataDir

  fileAndContents <- do
    let files = ["src/haskell-1.hs" , "src/haskell-2.hs" , "text-1.txt" , "text-2.txt"]
    M.fromList <$> forM files (\path -> (,) <$> pure path <*> BS.readFile path)

  hspec $
    describe "src4reader" $ do
      let subject cmdArgs input =
            E.withArgs (["-l", "*.hs/haskell"] ++ cmdArgs) $ do
              args <- prepareArguments $ C.yield $ unlines input
              C.runConduitRes $ mainC args .| C.sinkList

      let markdownWhenHaskell n =
            [ "# src/haskell-" <> n <> ".hs\n"
            , "```haskell\n"
            , fileAndContents M.! ("src/haskell-" <> BS.unpack n <> ".hs")
            , "```\n"
            ]

      let markdownWhenText n =
            [ "# text-" <> n <> ".txt\n"
            , "```\n"
            , fileAndContents M.! ("text-" <> BS.unpack n <> ".txt")
            , "```\n"
            ]

      context "given file paths from stdin" $
        it "returns the contents of the paths given from stdin" $ do
          let input = ["src/haskell-1.hs", "text-1.txt"]
              expected = markdownWhenHaskell "1" ++ markdownWhenText "1"
          subject ["--stdin"] input `shouldReturn` expected

      context "given paths as command line arguments" $
        it "returns the contents of the paths given from the arguments" $ do
          let input = ["src/", "text-2.txt"]
              expected =
                markdownWhenHaskell "1"
                  ++ markdownWhenHaskell "2"
                  ++ markdownWhenText "2"
          subject input [] `shouldReturn` expected

      context "given neither arguments nor stdin given" $ do
        it "returns the contents of the paths in current directory" $ do
          let expected =
                markdownWhenHaskell "1"
                  ++ markdownWhenHaskell "2"
                  ++ markdownWhenText "1"
                  ++ markdownWhenText "2"
          subject [] [] `shouldReturn` expected

        context "given --only-matched option" $
          it "returns the contents of the paths specified by the language option in current directory" $ do
            let expected =
                  markdownWhenHaskell "1" ++ markdownWhenHaskell "2"
            subject ["--only-matched"] [] `shouldReturn` expected
